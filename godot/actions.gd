extends Node

enum Rune {
	PRECISION = 0,
	VERTICALITY = 1,
	
	MOVEMENT = 2,
	PREHENSION = 3,
}

enum Action {
	GO = 200,
	CLIMB = 201,
	USE = 300,
	TAKEDROP = 301,
	INVALID=99999
}

func action_from_runes(a,b):	
	var prefix = a if a <= b else b
	var radix = b if a <= b else a
	# return radix*100+prefix # option 1
	# option 2
	match [prefix,radix]:
		[Rune.PRECISION,Rune.MOVEMENT]: return Action.GO
		[Rune.VERTICALITY,Rune.MOVEMENT]: return Action.CLIMB
		[Rune.PRECISION,Rune.PREHENSION]: return Action.USE
		[Rune.VERTICALITY,Rune.PREHENSION]: return Action.TAKEDROP
	return Action.INVALID

func runes_from_action(action):
	var radix = floor(action/100)
	var prefix = action % 100
	return [radix,prefix]

func _ready():
	if false:
		print("tests for actions.gd")
		print("should succeed GO ", Action.GO == action_from_runes(Rune.PRECISION, Rune.MOVEMENT))
		print("should fail GO ", Action.GO == action_from_runes(Rune.PRECISION, Rune.PREHENSION))
