extends FallingThing

signal player_got_key

func _on_PlayerDetector_body_entered(body):
	if body.is_in_group("player"):
		$sfx_collected.play()
		emit_signal("player_got_key", self)
