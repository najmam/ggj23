extends Node2D

signal reached_level_end
signal reached_last_level

var nb_keys = 0

func _ready():
	# handle signals emitted by entities
	for obj in $entities.get_children():
		if obj.is_in_group("key"):
			obj.connect("player_got_key", self, "_on_collected_key")
		elif obj.is_in_group("switch"):
			obj.connect("on_switch_changed", self, "_on_switch_changed")
		elif obj.is_in_group("door"):
			obj.connect("on_reached_door", self, "_on_reached_door")

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT and event.pressed:
			move_player_to(event.position)

# when the player collected a key, record the nb of keys then destroy the key
func _on_collected_key(key):
	nb_keys += 1
	key.queue_free()
	print("nb of keys you found : %d" % nb_keys)

func get_switch_targets():
	var res = []
	for obj in $entities.get_children():
		if obj.is_in_group("switch_target"):
				res.append(obj)
	return res

func _on_switch_changed(switch, status, target_name, activator):
	var found = false
	for obj in get_switch_targets():
		if obj.name == target_name:
			found = true
			if status == "on":
				obj.open()
			else:
				obj.close()
			break
	if not found:
		print("didn't find targetable for the switch that activates target '%s'" % target_name)

func _on_reached_door(next_level):
	if nb_keys == 0:
		print("found the door but you don't have any key!")
		return
	nb_keys -= 1
	print("reached door, go to the next level")
	
	if next_level == "last_level":
		emit_signal("reached_last_level")
	else:
		emit_signal("reached_level_end")

func move_player_to(position):
	($player as Node2D).position = position
