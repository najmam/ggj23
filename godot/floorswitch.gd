extends Node2D

signal on_switch_changed(switch, status, target_name, activator)

export var target_name : String = "no_target"
export var activated = false

onready var base_y = $Polygon2D.position.y

func _on_PlayerDetector_body_entered(body):
	if body.is_in_group("player") or body.is_in_group("chest"):
		emit_signal("on_switch_changed", self, "on", target_name, body)
		activated = true
		$sfx_activate.play()

func _on_PlayerDetector_body_exited(body):
	if body.is_in_group("player") or body.is_in_group("chest"):
		emit_signal("on_switch_changed", self, "off", target_name, body)
		activated = false
		$sfx_activate.play()

func _process(_delta):
	$Polygon2D.position.y = base_y + (2 if activated else 0)
