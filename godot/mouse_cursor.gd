extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#var standard_arrow = load("res://arrow.png")
var action1 = load("res://assets/cursor_action1.png")
var action2 = load("res://assets/cursor_action2.png")
var action3 = load("res://assets/cursor_action3.png")
var action4 = load("res://assets/cursor_action4.png")
var actions = [action1, action2, action3, action4]

# Called when the node enters the scene tree for the first time.
func _ready():
	#set_custom_mouse_cursor(null, CURSOR_ARROW)
	print("setting a new mouse icon for %s " % Input.get_current_cursor_shape())
	print("Input.CURSOR_ARROW = %d" %  Input.CURSOR_ARROW)
	Input.set_custom_mouse_cursor(action1) # for test
	Input.set_custom_mouse_cursor(action2, Input.CURSOR_ARROW, Vector2(16,16)) # for test
	Input.set_custom_mouse_cursor(action3, Input.CURSOR_BUSY, Vector2(16,16)) # for test
	Input.set_custom_mouse_cursor(action4, Input.get_current_cursor_shape(), Vector2(16,16)) # for test
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
