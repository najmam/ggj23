extends Node2D

export var next_scene = ""

func _ready():
	# skip title screen when in debug mode
	if next_scene == "game" and OS.is_debug_build():
		get_tree().change_scene("res://game.tscn")
	
	var anim = get_node("AnimationPlayer")
	if anim != null:
		anim.play("move")

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			var scene = "res://" + next_scene + ".tscn"
			get_tree().change_scene(scene)
