extends StaticBody2D

export var is_open = false

func _ready():
	pass

func _process(_delta):
	self.visible = !is_open
	$CollisionShape2D.disabled = is_open

func open():
	is_open = true
func close():
	is_open = false
func toggle():
	is_open = !is_open
