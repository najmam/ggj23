extends Node2D

#signal selection_changed(selected_runes)
signal action_changed(action)

var selected_slots = []

const keys = [KEY_S, KEY_D, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K, KEY_L]

const rune_width = 64
const runes_x_padding = rune_width*3/4
const max_nb_runes = 8
const runes_x_step = rune_width + rune_width/4

# load the mouse cursors 
var action1 = load("res://assets/cursor_action1.png")
var action2 = load("res://assets/cursor_action2.png")
var action3 = load("res://assets/cursor_action3.png")
var action4 = load("res://assets/cursor_action4.png")
var no_action = load("res://assets/cursor_no_action.png") # not used for the moment, we use null instead
var cursor_actions = {
	Actions.Action.INVALID : null,
	Actions.Action.GO : action1, Actions.Action.CLIMB : action2,
	Actions.Action.USE : action3, Actions.Action.TAKEDROP :action4
}

var cur_action = Actions.Action.INVALID

func get_runes():
	return get_node("..").cur_level.get_node("runes")

func update_cursor(id):
	if id == Actions.Action.INVALID:
		Input.set_custom_mouse_cursor(cursor_actions[id], Input.CURSOR_ARROW, Vector2(0,0))
	else:
		Input.set_custom_mouse_cursor(cursor_actions[id], Input.CURSOR_ARROW, Vector2(16,16))

func rune_at_slot(idx):
	var runes = get_runes().get_children()
	if idx >= len(runes):
		return -1
	return runes[idx].rune_type

func select_rune_at_slot(slot_idx):
	#print("selected rune at slot %s" % slot_idx)
	var rune = rune_at_slot(slot_idx)
	if rune == -1:
		print("warning: asked for rune at slot %d" % slot_idx)
		return
	return rune

func _input(event):
	# pressing on a key selects the rune at that slot
	var key_pressed = false
	var changed = false
	for i in range(0, len(keys)):
		if Input.is_physical_key_pressed(keys[i]):
			key_pressed = true
#			print("key %d pressed" % i)
			# if the rune/slot is already selected, we do nothing
			if i in selected_slots:
				print("slot %d already selected, doing nothing" % i)
				return
			selected_slots.append(i)
			changed = true
	# if we have more than 2 runes selected, we deselect the last one
	if len(selected_slots) >= 3:
		print("deselecting slot %d" % selected_slots[0])
		selected_slots.remove(0)

	if changed:
		propagate_selected_slots()

# emit signals and update the cursor
func propagate_selected_slots():
	# once we have updated the selected_slots, we translate them to runes
	var selected_runes = slots_to_runes(selected_slots)
	print("selected slots/runes:", selected_slots, selected_runes)
#	emit_signal("selection_changed", selected_runes)

	var prev_action = cur_action
	
	if len(selected_runes) == 2 and selected_runes[0] != null and selected_runes[1] != null:
		cur_action = Actions.action_from_runes(selected_runes[0],selected_runes[1])
	else:
		cur_action = Actions.Action.INVALID
	
	if cur_action != prev_action:
		emit_signal("action_changed", cur_action)
		update_cursor(cur_action)
		
# update the selected_runes array as well as runes/slot animations 
func slots_to_runes(selection):
	var selected_runes = []
	for i in range(0,len(get_runes().get_children())):
		var rune_obj = get_runes().get_children()[i]
		var animation_player = rune_obj.get_node("AnimationPlayer") as AnimationPlayer
		var rune_type = select_rune_at_slot(i)
		if i in selection:
			selected_runes.append(rune_type)
#			print ("updated selected_runes : ", selected_runes)
			animation_player.current_animation = 'selected'
		else:
			animation_player.current_animation = 'RESET'
	return selected_runes

# align the runes neatly in the HUD
func _process(_delta):
	# we only display letters under the runes
	var letters = get_node("letters").get_children()
	var len_letters = len(letters)
	var len_runes = len(get_runes().get_children())

	for i in range(0, len_letters):
		letters[i].visible = true if i < len_runes else false

	for i in range(0, len(get_runes().get_children())):
		var rune = get_runes().get_children()[i] as Node2D
		rune.position.x = runes_x_padding + i*runes_x_step
		rune.position.y = 2
		#print(i, " ", rune.position.x, " ", rune.rune_type)

# when the action target changes, move the crosshair to it
func _on_action_target_changed(action_target):
	if action_target == null:
		$crosshair.visible = false
	else:
		$crosshair.visible = true
		$crosshair.position = (action_target as Node2D).global_position
		$crosshair/AnimationPlayer.play("RESET")

func _on_action_succeeded(action, target):
	# delete the runes that were selected
	var children_to_delete = []
	for i in range(0, len(selected_slots)):
		var child = get_runes().get_children()[selected_slots[0]]
		children_to_delete.append(child)
		selected_slots.remove(0)
	for child in children_to_delete:
		child.queue_free()
	
	propagate_selected_slots()

func _on_level_changed():
	for slot in range(0,len(selected_slots)):
		selected_slots.remove(0)
	propagate_selected_slots()
