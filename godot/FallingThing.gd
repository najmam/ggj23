class_name FallingThing
extends KinematicBody2D

# inherited by Player and Key

export var speed = Vector2(150.0, 350.0)
onready var gravity = ProjectSettings.get("physics/2d/default_gravity")

const FLOOR_NORMAL = Vector2.UP

const FLOOR_DETECT_DISTANCE = 20.0

var _velocity = Vector2.ZERO

# _physics_process is called after the inherited _physics_process function.
# This allows the Player and Enemy scenes to be affected by gravity.
func _physics_process(delta):
	_velocity.y += gravity * delta
	
	var snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE
	var is_on_platform = $PlatformDetector.is_colliding()
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, FLOOR_NORMAL, not is_on_platform, 4, 0.9, false
	)
