extends Node2D

signal action_target_changed(action_target)
signal action_succeeded(action, target)
signal level_changed

const ACTION_GO_MAXIMUM_JUMP_HEIGHT = 25

var cur_action_target = null
var last_mouse_pos = Vector2.ZERO
onready var cur_level = $levels/level1
var cur_level_name = "level1"

func _input(event):
	# when the mouse moves, update the current target highlighted by the HUD
	if event is InputEventMouseMotion:
		last_mouse_pos = event.position
		update_action_target()
	# when the mouse is clicked while we have an action target, perform that action
	elif event is InputEventMouseButton \
		and event.button_index == BUTTON_LEFT \
		and event.pressed \
		and cur_action_target != null:
			perform_action()
	elif event.is_action_pressed("restart_game"):
		restart_game()
	elif event.is_action_pressed("restart_level"):
		restart_level()
	elif event.is_action_pressed("skip_level"):
		go_to_next_level()

#################### changing levels

func _ready():
	cur_level.connect("reached_level_end", self, "go_to_next_level")
	cur_level.connect("reached_last_level", self, "_on_reached_last_level")

func change_level(next_level):
	print("change to level %s" % next_level)
	var obj = load("res://" + next_level + ".tscn").instance()
	$levels.add_child(obj)
	cur_level.queue_free()
	cur_level = obj
	cur_level_name = next_level
	cur_level.connect("reached_level_end", self, "go_to_next_level")
	cur_level.connect("reached_last_level", self, "_on_reached_last_level")
	emit_signal("level_changed")

func go_to_next_level():
	change_level(cur_level.get_node("entities/door").next_level)

func restart_level():
	change_level(cur_level_name)

func restart_game():
	change_level("level1")

func _on_reached_last_level():
	get_tree().change_scene("res://creditsScreen.tscn")

#################### actions

func _on_action_changed(action):
#	print("action changed ", action)
	update_action_target()

func update_action_target():
	var cur_action = $hud.cur_action
	var target = null
	if cur_action != Actions.Action.INVALID:
		target = locate_action_target(last_mouse_pos, cur_action)
		if target != cur_action_target:
			cur_action_target = target
	emit_signal("action_target_changed", target)

func locate_action_target(mouse_pos, cur_action):
	# let's grab all the entities matching the group concerned by that action
	var entity_group_name = ""
	match cur_action:
		Actions.Action.GO: entity_group_name = "action_go"
		Actions.Action.CLIMB: entity_group_name = "action_climb"
		Actions.Action.USE: entity_group_name = "action_use"
		Actions.Action.TAKEDROP: entity_group_name = "action_take"
	var entities = get_tree().get_nodes_in_group(entity_group_name)
	var nb = len(entities)
#	print("moving the mouse with action %s, there are %d entities" % [entity_group_name,nb])
	
	# if there are no available entities, we have no target
	if nb == 0:
		return null

	# find the target nearest to the mouse cursor
	var nearest = null
	var min_distance = 99999
	for obj in entities:
		var distance = ((obj as Node2D).global_position - (mouse_pos as Vector2)).length()
		if distance < min_distance:
			nearest = obj
			min_distance = distance
	
	return nearest
	
func perform_action():
	var action = $hud.cur_action
	var target = cur_action_target
	if action == Actions.Action.INVALID or target == null:
		print("WARNING got into perform_action with invalid state")
	
	var performed = false
	
	var player = cur_level.get_node("player")
	
#	print("perform action %s on %s" % [action, target])
	if action == Actions.Action.GO:
		var dy = abs(player.get_node("feet").global_position.y - target.global_position.y)
		var can_go = dy <= ACTION_GO_MAXIMUM_JUMP_HEIGHT
		print("GO dy: %d (%s)" % [dy, "can go" if can_go else "can't go"])
		if can_go:
			cur_level.move_player_to(target.global_position)
			performed = true
	elif action == Actions.Action.CLIMB:
		var direction = "up" if player.global_position.y > target.global_position.y  else "down"
		var dest = target.get_node("destination_" + direction)
		if dest != null:
			cur_level.move_player_to(dest.global_position - player.get_node("feet").position)
			performed = true
	elif action == Actions.Action.TAKEDROP:
		pass
	elif action == Actions.Action.USE:
		pass
	
	if performed:
		emit_signal("action_succeeded", action, target)

####################

