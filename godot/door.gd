extends Node2D

signal on_reached_door(next_level)

export var next_level = "no_level"

func _on_PlayerDetector_body_entered(body):
	if body.is_in_group("player"):
		emit_signal("on_reached_door", next_level)
