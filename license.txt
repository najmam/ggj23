Source: GNU GPLv3, see GPLv3.txt
Assets: Creative Commons BY, see https://creativecommons.org/licenses/by/2.0/fr/

Thirdparty assets:
- Monogram font https://datagoblin.itch.io/monogram/

Credits:
- dev: najmam (Naji Mammeri)
- dev: pixjuan
- art: liquid_vessel
- sound design: superjesus2