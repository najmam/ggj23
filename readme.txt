Vous incarnez Jammy Jam, le cousin jamaïcain de Jean-Michel Jam.
Malheureusement, Jammy a tellement fait la fête que ses dreadlocks se sont déracinées et détachées.
Aidez Jammy à retrouver ses dreads en le guidant à travers les vastes salles de son palace caribéen.

## Comment jouer

Vous finissez un niveau en guidant Jammy à travers la porte.
Pour ouvrir la porte, il faut d'abord avoir trouvé la clé.
Pour trouver la clé, il va falloir vous déplacer, grimper, soulever des trucs, et utiliser des trucs.
Comme Jammy a trop fait la fête, vous devez l'aider à se rappeler comment faire toutes ces actions.

Pour effectuer une action, sélectionner (touches S-D-F-G-H-J-K-L) les deux Runes auxquelles elle correspond :
- Grimper : rune Mouvement + rune Verticalité
- se Déplacer : rune Mouvement + rune Précision
- Soulever : rune Préhension + rune Verticalité
- Utiliser : rune Préhension + rune Précision

Une fois deux runes compatibles sélectionnées, le curseur change pour indiquer l'action en cours. (Ne fonctionne pas en version web)
Déplacer la souris pour changer la cible de l'action.
Cliquer pour effectuer l'action.
Si ça a fonctionné, les runes de l'action sont consommées et disparaissent du niveau.
Utilisez les bonnes runes au bon moment pour ne pas vous retrouver bloqués !

## Contrôles

- touches S-D-F-G-H-J-K-L pour sélectionner une rune
- clic gauche pour effectuer l'action correspondant aux runes sélectionnées
- R pour redémarrer le niveau
- T pour sauter le niveau
- P pour redémarrer le jeu
- (clic droit : God Mode, permet de téléporter Jammy. Ne pas utiliser, c'est pour les dévs, et surtout la téléportation c'est pas roots)
